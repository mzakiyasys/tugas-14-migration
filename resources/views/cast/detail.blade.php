@extends('layout.master')

@section('judul')
    Detail Data
@endsection

@section('content')

<h1>{{$cast->nama}}, {{$cast->umur}} Tahun</h1>
<p>{{$cast->bio}}</p> 

<a href="/cast" class="btn btn-secondary btn-sm">Back</a>

@endsection

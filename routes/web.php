<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/data-table', function(){
    return view('halaman.datatable');
});

Route::get('/table', function(){
    return view('halaman.table');
});


//CRUD cast
//Create
//Form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
//kirim ke database
Route::post('/cast', [CastController::class, 'store']);

//Read
Route::get('/cast', [CastController::class, 'index']);
//Detail Cast by ID
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update
//Form Update Cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Update by ID
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete
//Delete by ID
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);




